package form

type SocialRequest struct {
	Token      string `json:"token"`
	Newsletter bool   `json:"newsletter"`
}
