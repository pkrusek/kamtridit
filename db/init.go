package db

import (
	"context"
	"os"
	"time"

	log "github.com/sirupsen/logrus"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

type Resource struct {
	DB *mongo.Database
}

func InitResource() (*Resource, error) {
	databaseConn := os.Getenv("DATABASE_CONN")

	mongoClient, err := mongo.NewClient(options.Client().ApplyURI(databaseConn))
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)

	defer cancel()

	err = mongoClient.Connect(ctx)
	if err != nil {
		return nil, err
	}

	return &Resource{DB: mongoClient.Database("data")}, nil
}

func (r *Resource) Close() {
	log.Warning("Closing all db connections")
}
