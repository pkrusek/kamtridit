package api

import (
	"kamtridit/app/repository"
	"kamtridit/db"
	"kamtridit/form"
	"kamtridit/utils/bcrypt"
	"log"
	"net/http"

	"fmt"

	"github.com/antonholmquist/jason"
	"github.com/gin-gonic/gin"
	"github.com/monaco-io/request"
)

func ApplyAuthAPI(app *gin.RouterGroup, resource *db.Resource) {
	userEntity := repository.NewUserEntity(resource)
	authRoute := app.Group("/auth")
	authRoute.POST("/login", login(userEntity))
	authRoute.POST("/facebook", loginFacebook(userEntity))
}

func login(userEntity repository.IUser) func(ctx *gin.Context) {
	return func(ctx *gin.Context) {
		userRequest := form.User{}
		if err := ctx.ShouldBindJSON(&userRequest); err != nil {
			ctx.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
			return
		}

		user, code, _ := userEntity.GetUserByEmail(userRequest.Email)

		if user == nil {
			ctx.AbortWithStatusJSON(http.StatusNotFound, gin.H{"err": fmt.Sprintf("Uživatel s e-mailem %s nenalezen.", userRequest.Email)})
			return
		}

		if bcrypt.ComparePasswordAndHashedPassword(userRequest.Password, user.Password) != nil {
			ctx.AbortWithStatusJSON(http.StatusForbidden, gin.H{"err": "Špatné heslo."})
			return
		}

		response := map[string]interface{}{
			"email": user.Email,
		}

		ctx.JSON(code, response)
	}
}

func loginFacebook(userEntity repository.IUser) func(ctx *gin.Context) {
	return func(ctx *gin.Context) {
		client := request.Client{
			URL:    "https://graph.facebook.com/me",
			Method: "GET",
			Params: map[string]string{"fields": "id, email, name, first_name, middle_name, last_name, picture.width(720).height(720), short_name", "access_token": "EAATrbBaah4wBAIjNwBei35n478yIJEdjoe1LZCn2eQZBZABq1UaXNI5Y6c0cLtsjhup2w9lhokOa82NWZC54SqCyqkVgFv8xfoerFk1ZB6CNUoiCXv2lgzB29i6Sg6r9XULYVeUeZA0iqMAuYfbZCupspD365OBZCmEhQZACcHH9UVQsowEZBoYnVGqDV1pI0pDpw1NZB6w5ynNQjRvar8LCimsDcZB4cYM9CgEZD"},
		}
		resp, err := client.Do()

		if err != nil {
			return
		}

		//log.Print(resp.Code, string(resp.Data), err)

		user, _ := jason.NewObjectFromBytes(resp.Data)

		pictureObject, err := user.GetObject("picture", "data")
		if err != nil {
			return
		}
		avatar, _ := pictureObject.GetString("url")

		log.Print(avatar)

		ctx.JSON(200, "{}")
	}
}

// public async login(data: any) {
// 	const userData = data as User
// 	const user = await this.user.findOne({ email: userData.email })
// 	if (!user) throw new UserNotFoundException(userData.email)
// 	if (user.loginType != 'login' && !user.password) {
// 		throw new WrongLoginTypeException(user.loginType)
// 	}
// 	if (!user.isVerified) throw new UserNotVerifiedException()
// 	const validPwd = await Bcrypt.compare(userData.password, user.password)
// 	if (!validPwd) throw new WrongCredentialsException('Špatné heslo.')
// 	return await this.saveUser(user)
// }
