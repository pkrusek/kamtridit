package app

import (
	"kamtridit/app/api"
	"kamtridit/db"

	helmet "github.com/danielkov/gin-helmet"
	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
)

type Routes struct {
}

func (app Routes) StartApp() {
	r := gin.Default()

	r.Use(helmet.Default())

	apiRoute := r.Group("/api")

	resource, err := db.InitResource()
	if err != nil {
		logrus.Error(err)
	}
	defer resource.Close()

	api.ApplyAuthAPI(apiRoute, resource)

	r.Run()
}
