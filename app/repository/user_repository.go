package repository

import (
	"kamtridit/db"
	"kamtridit/model"
	"net/http"

	log "github.com/sirupsen/logrus"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
)

var UserEntity IUser

type userEntity struct {
	resource *db.Resource
	repo     *mongo.Collection
}

type IUser interface {
	GetUserByEmail(email string) (*model.User, int, error)
}

func NewUserEntity(resource *db.Resource) IUser {
	userRepo := resource.DB.Collection("users")
	UserEntity = &userEntity{resource: resource, repo: userRepo}

	return UserEntity
}

func (entity *userEntity) GetUserByEmail(email string) (*model.User, int, error) {
	ctx, cancel := initContext()
	defer cancel()

	var user model.User
	err := entity.repo.FindOne(ctx, bson.M{"email": email}).Decode(&user)

	if err != nil {
		log.Print(err)
		return nil, 400, err
	}

	return &user, http.StatusOK, nil
}
