module kamtridit

go 1.14

require (
	github.com/0xAX/notificator v0.0.0-20191016112426-3962a5ea8da1 // indirect
	github.com/antonholmquist/jason v1.0.0
	github.com/codegangsta/envy v0.0.0-20141216192214-4b78388c8ce4 // indirect
	github.com/codegangsta/gin v0.0.0-20171026143024-cafe2ce98974 // indirect
	github.com/danielkov/gin-helmet v0.0.0-20171108135313-1387e224435e
	github.com/dgrijalva/jwt-go v3.2.0+incompatible // indirect
	github.com/gin-gonic/gin v1.6.3
	github.com/joho/godotenv v1.3.0
	github.com/mattn/go-shellwords v1.0.10 // indirect
	github.com/monaco-io/request v1.0.5
	github.com/sirupsen/logrus v1.4.2
	go.mongodb.org/mongo-driver v1.4.4
	golang.org/x/crypto v0.0.0-20190530122614-20be4c3c3ed5
	gopkg.in/urfave/cli.v1 v1.20.0 // indirect
)
