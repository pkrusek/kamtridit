# kamtridit.cz
Project for Ekokom, a.s.

# Author
* [Pavel Krusek](mailto://pavel.krusek@gmail.com)
# Technologies
* [Go - A statically typed, compiled programming language designed at Google](https://golang.org)
* [Gin - A web framework written in Go](https://github.com/gin-gonic/gin)
* [MongoDB - A document-based database](https://www.mongodb.com)
* [Redis - An in-memory data structure store, used as a distributed, in-memory key–value database](https://redis.io)
* [React - A JavaScript library for building user interfaces](https://reactjs.org)

# Set up
* Create file .env
