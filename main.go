package main

import (
	"kamtridit/app"
)

func main() {
	var server app.Routes
	server.StartApp()
}
